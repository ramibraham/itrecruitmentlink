﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class pidevdbEntities : DbContext
    {
        public pidevdbEntities()
            : base("name=pidevdbEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<candidature> candidatures { get; set; }
        public DbSet<category> categories { get; set; }
        public DbSet<@event> events { get; set; }
        public DbSet<joboffer> joboffers { get; set; }
        public DbSet<meeting> meetings { get; set; }
        public DbSet<speeker> speekers { get; set; }
        public DbSet<t_certification> t_certification { get; set; }
        public DbSet<t_curriculumvitae> t_curriculumvitae { get; set; }
        public DbSet<t_experience> t_experience { get; set; }
        public DbSet<t_qualification> t_qualification { get; set; }
        public DbSet<type> types { get; set; }
        public DbSet<user> users { get; set; }
        public DbSet<virtualmeeting> virtualmeetings { get; set; }
    }
}
