﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class StudentDAO
    {
        #region Singleton
        public static readonly object myLock = new object();
        private static StudentDAO instance = null;

        private StudentDAO() { }

        public static StudentDAO getInstance()
        {
            lock (myLock)
            {
                if (instance == null)
                    instance = new StudentDAO();
                return instance;

            }
        }
        #endregion


        #region CRUD
        public static List<user> getAllStudents()
        {
            using (pidevdbEntities ec = new pidevdbEntities())
            {
                return ec.users.ToList<user>();
            }
        }

        
        
        #endregion

    }
}

