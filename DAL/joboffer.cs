//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class joboffer
    {
        public joboffer()
        {
            this.candidatures = new HashSet<candidature>();
        }
    
        public int id { get; set; }
        public string contractType { get; set; }
        public byte[] description { get; set; }
        public Nullable<System.DateTime> postDate { get; set; }
        public int postsNumber { get; set; }
        public double salary { get; set; }
        public string status { get; set; }
        public string title { get; set; }
        public string workPlace { get; set; }
    
        public virtual ICollection<candidature> candidatures { get; set; }
    }
}
